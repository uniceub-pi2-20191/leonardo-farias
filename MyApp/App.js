import React, {Component} from 'react';
import {Image, Text, ScrollView, StyleSheet, View} from 'react-native';

const my_style = StyleSheet.create({
  mainLayout: {
    flex: 1,
    marginTop: 30,
    alignItems: 'center',
  },
  textTitle: {
    fontSize: 30,
    color: '#000'
  },
  textParagraph: {
    flex: 1,
    fontSize: 16,
    color: '#222'
  },
});



export default class HelloReactNative extends Component {
  render() {
    return (
      <ScrollView>
        <Text style = {my_style.textTitle}>
          Os beneficios da cebola.
        </Text>
        <Image style = {{maxWidth: 300, height: 400, marginLeft: 30, marginRight: 10, marginBottom: 20, marginTop: 20}} source={require('./cebola.jpg')} />
        <Text style = {my_style.textParagraph}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sed neque urna. 
          Vivamus quam ligula, molestie sit amet neque non, auctor vulputate dolor. Sed 
          efficitur maximus feugiat.{'\n'}{'\n'}
          Maecenas placerat ex a ante mollis viverra eget vel mauris. 
          Proin nec est enim. Praesent ullamcorper turpis in erat hendrerit, sed aliquam erat pulvinar. 
          Nunc porta libero sit amet faucibus auctor. Praesent orci augue, lobortis aliquet tristique sit amet, 
          volutpat vel odio.{'\n'}{'\n'}
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque sed neque urna. 
          Vivamus quam ligula, molestie sit amet neque non, auctor vulputate dolor. Sed 
          efficitur maximus feugiat.
        </Text>
      </ScrollView>
    );
  }
}